<?php

require_once 'src/Feed.php';

$rss = Feed::loadRss('https://engage.colum.edu/rss_events?group_ids=66244');

?>

<h1><?php echo htmlspecialchars($rss->title) ?></h1>

<p><i><?php echo htmlspecialchars($rss->description) ?></i></p>

<?php foreach ($rss->item as $item): ?>
	<h2><a href="<?php echo htmlspecialchars($item->url) ?>"><?php echo htmlspecialchars($item->title) ?></a>
	<img src="<?php echo htmlspecialchars($item->eventPhotoFullUrl) ?>"  />
	<small><?php echo $item->eventDate ?></small></h2>
    <!-- <eventTime>8:00am</eventTime>
<eventEndTime>5:00pm</eventEndTime> -->

	<?php if (isset($item->{'content:encoded'})): ?>
		<div><?php echo $item->{'content:encoded'} ?></div>
	<?php else: ?>
		<p><?php echo htmlspecialchars($item->description) ?></p>
	<?php endif ?>
<?php endforeach ?>