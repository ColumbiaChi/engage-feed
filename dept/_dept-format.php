<xsl:processing-instruction name="php">
    <!-- $FILENAME = 'events_
    <xsl:value-of select="oasis-code"/>'; -->
    $FEED_URL = 'https://engage.colum.edu/rss_events?group_ids=
    <xsl:value-of select="localist-id"/>&amp;future_day_range=365&amp;distinct=true&amp;limit=4';
    <!-- $EXPIRE_IN_HOURS = 1; -->
    $more_events_url = 'https://engage.colum.edu/events?group_ids=
    <xsl:value-of select="localist-id"/>';
    $dateformat = 'F d, Y';
    $timeformat = 'g:i A';
    $description_word_limit = 55;
    $hide_type = array();
    $content = '';
    $items = array();
    $prev_date = '';    
    include($_SERVER['DOCUMENT_ROOT'].'/php/_event-list.php');


</xsl:processing-instruction>