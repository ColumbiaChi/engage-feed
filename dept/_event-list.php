<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// dev
require_once '../vendor/Feed.php';
//prod 
// include_once($_SERVER['DOCUMENT_ROOT'].'/php/_functions.php');
// include_once($_SERVER['DOCUMENT_ROOT'].'/php/rssforphp.php');



// Feed::$cacheDir = __DIR__ . '/cache';
// Feed::$cacheExpire = '5 hours';

//hardwired for dev
$feed = Feed::loadRss('https://engage.colum.edu/rss_events?group_ids=66244&limit=4');
//for production
// $feed = Feed::loadRss($FEED_URL);

//for dev
$more_events_url = 'https://engage.colum.edu/events?group_ids=66244';
$description_word_limit = 55;
function wordcount($description,$wordcount = 0) {
  if ($wordcount > 0) {
      $explode = explode(' ',$description);
      $explode = array_slice($explode,0,$wordcount);
      $implode = implode(' ',$explode);
      return $implode.'...';
  } else {
      return $description;
  }
}




$hide_type = array(10234);
$dateformat = 'l, F d, Y';
$timeformat = 'g:iA';
$content = '';
$count = 0;



if(count($feed->item) < 1) {

  $content = '<p>There are no upcoming events at this time.</p>';

} else {
  $content = '<ul class="event-list no-bullet">';
    
  // parse through events and store output
  foreach($feed->item as $e) {
    // clear temporary event storage (for re-ordering)
    $item = '';
    // check if we are ignoring this event type
    // $ignore_flag = 0;
    // for($t=0; $t < count($e->event->filters->event_types); $t++) {
    //   if (in_array($e->event->filters->event_types[$t]->id, $hide_type)) {
    //       $ignore_flag = 1;
    //   }    
    // }
    
        $start = $e->start;
        $end = $e->eventEndTime;
        $date =  date($dateformat,strtotime($start));
        $start_time = date($timeformat,strtotime($start));
        if ($end == null) {
            $end_time = null;
        } else {
            $end_time = date($timeformat,strtotime($end));
        }
        $all_day = $e->allDayEvent;
        
      
        // if ($date != $prev_date) {
            $item .= '<li>';
              $item .= '<a href="'.$e->link.'">';
                $item .= '<span>';
                  $item .= '<meta itemprop="startDate" content="' . $start . '">';
                //   if ($e->event->first_date != $e->event->last_date) {
                //     $item .= '<div class="more-dates">More Dates</div>';
                //   }
                  $item .= '<aside class="date">';
                    $item .= '<span class="month">' . date('M',strtotime($start)) . '</span>';
                    $item .= '<span class="date">' . date('d',strtotime($start)) . '</span>';
                  $item .= '</aside>';
            $prev_date = $date;
        // } else {
        //     $item .= '<li class="same-date">';
        //       $item .= '<a href="#">';
        //         $item .= '<span>';
        //           $item .= '<meta itemprop="startDate" content="' . $start . '">';
        // }
      
        if($all_day !== 0) {
            $item .= '<time>' . $start_time;
            if ($end_time != null) {
              $item .= ' - ' . $end_time;                                       
            }
            $item .= '</time>'; 
        } else {
            $item .= '<time>ALL DAY</time>';
        }
        $item .= ' <span itemprop="name" class="event-name">' . $e->title . '</span>';
        $item .= '</a>';
        $item .= '<div class="more">';
          if (!isset($hide_photo) || $hide_photo == false) {
              $item .= '<div class="thumbnail">';
                $item .= '<a href="' . $e->link . '" style="background-image: url('.$e->eventOriginalPhotoFullUrl.')"></a>';
              $item .= '</div>';     
          }
          // if (($e->eventExternalRegistrationLink != '') && $show_register_button) {
          //     $item .= '<a class="small button right" target="_blank" href="' . $e->event->ticket_url . '">Register</a>';
          // }
          // if ($e->eeventLocation != '') {
          //   if ($e->event->venue_url != '') {
          //       $item .= '<a class="location" target="_blank" href="'. $e->event->venue_url .'">';    
          //   } else {
          //       $item .= '<a class="location" target="_blank" href="https://www.google.com/maps/search/'. $e->event->location_name .'/@'. $e->event->geo->latitude .','. $e->event->geo->longitude .',15z">';    
          //   }
          //     $item .= '<div itemscope itemtype="http://schema.org/Place">
          //                 <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

          //                   <span itemprop="streetAddress">
          //                       '.$e->event->geo->street.', '.$e->event->room_number.'
          //                   </span>';
          //     if (($e->event->geo->city != '')) {
          //       $item .= '<span itemprop="addressLocality">'.$e->event->geo->city.'</span>,
          //                 <span itemprop="addressRegion">'.$e->event->geo->state.'</span>
          //                 <span itemprop="postalCode">'.$e->event->geo->zip.'</span>';                            
          //     }
          //     $item .= '<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
          //                     <meta itemprop="latitude" content="' . $e->event->geo->latitude . '">
          //                     <meta itemprop="longitude" content="' . $e->event->geo->longitude . '">
          //                   </div>
          //                 </div>
          //               </div>';
          //   $item .= '</a>';
          // }
          if (!isset($hide_description) || $hide_description == false) {
              $item .= '<span itemprop="description">' . wordcount($e->description, $description_word_limit) . '</span>';    
          }

          $item .= '<a class="close"></a>';
          if (!isset($hide_details_link) || $hide_details_link == false) {
              $item .= '<span class="read-more"><a target="_blank" href="' . $e->link . '">Get the details</a></span>';
          }
        $item .= '</div>';
      $item .= '</li>';
      $items[] = $item;
    
  }
  //$reversed = array_reverse($items);
  foreach($items as $e) {
    $content .= $e;
  }
  $content .= '<footer><a href="' . $more_events_url . '">More Upcoming Events</a></footer>';
  $content .= '</ul>';
}

// output html
echo $content;

?>
