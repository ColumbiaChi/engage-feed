
<html>
<head>
    <link href="https://www.colum.edu/css/app.css?v=13" rel="stylesheet" />
</head>
<body>
    <p>dept rss feed example</p>
<div>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('./_event-list.php');
?>
<script src="https://www.colum.edu/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="https://www.colum.edu/bower_components/jquery-smooth-scroll/jquery.smooth-scroll.min.js" type="text/javascript"></script>
<script>
var slidetoggle_speed = 'medium';

     // event widget

     $('.event-list > li > a').click(function(event) {

event.preventDefault();

$('.event-list .close').hide();

$(this)

    .next()

    .slideToggle(slidetoggle_speed, function() {

        if ($(this).is(':visible')) {

            $(this)

                .parent()

                .addClass('active');

            $(this)

                .find('.close')

                .fadeIn();

            // scroll to expanded event

            $.smoothScroll({

                easing: smoothscroll_easing,

                speed: smoothscroll_speed,

                scrollTarget: $(this).parent()

            });

        } else {

            $(this)

                .parent()

                .removeClass('active');

        }

    });

// slideToggle other expanded events

$('.event-list > li > a')

    .not(this)

    .parent()

    .find('.more[style="display: block;"]')

    .slideToggle()

    .parent()

    .removeClass('active');

});

$('.event-list .more .close').click(function(e) {

e.preventDefault();

$(this).fadeOut();

$(this)

    .parent()

    .slideToggle()

    .parent()

    .removeClass('active');

});
</script>

</div>
</body>
</html>