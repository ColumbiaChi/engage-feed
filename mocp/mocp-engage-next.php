
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '../vendor/Feed.php';
echo 'something';
// Feed::$cacheDir = __DIR__ . '/cache';
// Feed::$cacheExpire = '5 hours';

$rss = Feed::loadRss('https://engage.colum.edu/rss_events?group_ids=66244');

$hide_type = array(10234);
$dateformat = 'l, F d, Y';
$timeformat = 'g:iA';
$content = '';
$count = 0;

foreach ($rss->item as $e) {
	if($count<1) {
        $count++;
        // $e = $e->event;
        $date = $e->eventDate;
        // $time = date($timeformat,strtotime($date));
        // offset one hour to account for Localist server time
        $time = date($timeformat,strtotime($date . $e->eventTime));
        $xdate = explode("T", $date);
        $date = array_shift($xdate);
        $date =  date($dateformat,strtotime($date));
        $content .= '<article id="localist-event">';
        $content .= '<header>';
        $content .= '<h1>'.$e->title.'</h1>';
        $content .= '<time>'.$date.'</time>';
        $content .= '<time class="start-time"> <span>'.$time.'</span></time>';
        $content .= '<h2 class="location">';
        $content .= $e->location_name;
        if ($e->address != '') {
        $content .= ', <span>'.$e->address.'</span>';
        }
        $content .= '</h2>';
        $content .= '</header>';
        $content .= '<img alt="'.$e->title.'" src="'.$e->eventOriginalPhotoFullUrl.'"/>';
        $content .= '<div class="body">';
        //$content .= '<p>'. nl2br($e->description).'</p>';
        $content .= $e->fullDescription;

        // no ticket cost field to map
        // if($e->ticket_cost != '') {
        // $content .= '<p>';
        // $content .= '<a class="button" href="'.$e->ticket_url.'">Buy Tickets</a>';
        // // $content .= '<small>'.$e->ticket_cost.'</small>';
        // $content .= '</p>';

        if ($e->link != '') {
            $content .= '<p>';
            $content .= '<a class="button" href="'.$e->link.'">Register</a>';
            $content .= '</p>';    
            }
            $content .= '</div>';
            $content .= '</article>';
    }
}
    

// output html
echo $content;

?>